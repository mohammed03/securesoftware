# coding=utf-8

import time
import datetime

tags = {
	"error": "X ",
	"info": "  "
}


class Log:

	@staticmethod
	def time():
		ts = time.time()
		st = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
		return st

	@staticmethod
	def error(message):
		print(tags["error"] + Log.time() + " " + message)

	@staticmethod
	def add(message):
		print(tags["info"] + Log.time() + " " + message)
