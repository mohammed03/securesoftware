from Log import *
from threading import Thread
import socket
import sqlite3
from sqlite3 import Error
import rsa
from Crypto.Cipher import AES
import json

class Server:

	def __init__(self, port: int, keyfile: str):
		Log.add("Server creation")
		self.__port = port
		self.__key = rsa.PrivateKey.load_pkcs1(open(keyfile).read())
		self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.__is_running = False
		self.__connected_clients = []


	def start(self):
		if not self.__is_running:
			self.__is_running = True
			self.__socket.bind(('', self.__port))
			self.__thread_receive = Thread(target=self.__thread_receive_fct)
			self.__thread_receive.start()
			db_conn = self.__create_connection("database.db")

			if db_conn is not None:
				# create users table
				sql_create_users_table = """ CREATE TABLE IF NOT EXISTS users (
				                                    id text PRIMARY KEY,
				                                    password_hash text NOT NULL,
				                                    password_salt text NOT NULL,
				                                    pubkey text NOT NULL
				                                ); """
				self.__create_table(db_conn, sql_create_users_table)
			else:
				print("Error! cannot create the database connection.")
			db_conn.close()

	def __create_connection(self, db_file):
		try:
			return sqlite3.connect(db_file)
		except Error as e:
			print(e)

	def __create_table(self, conn, create_table_sql):
		try:
			c = conn.cursor()
			c.execute(create_table_sql)
		except Error as e:
			print(e)

	def __thread_receive_fct(self):
		db_conn = self.__create_connection("database.db")
		while self.__is_running:
			data, addr = self.__socket.recvfrom(65507)
			
			print("addr: " + str(addr))

			data_dict = json.loads(data.decode())
			message = bytes.fromhex(data_dict["message"])
			key = bytes.fromhex(data_dict["key"])

			aes_key = rsa.decrypt(key, self.__key)
			Log.add("key decrypted: " + str(aes_key))
			
			aes_suite = AES.new(aes_key, AES.MODE_CBC, '0123456789abcdef')
			decrypted_message = aes_suite.decrypt(message).decode()
			message_dict = json.loads(decrypted_message.rstrip("0"))
			Log.add("Message: " + str(message_dict))

			type = message_dict["type"]
			
			if type == "register":
				self.registering_client(db_conn, message_dict)

			elif type == "connect":
				name = message_dict["login"]
				port = message_dict["port"]
				print("name:" + name)

				# try:
				cur = db_conn.cursor()
				user = cur.execute(cur.execute("SELECT * FROM users WHERE id=?", (name,)))

				print("user: " + user)
				# except:
				# 	print("User look up failed")
				

	def registering_client(self, db_conn, message_dict):
		name = message_dict["login"]
		hash = message_dict["hashed_password"]
		salt = message_dict["salt"]
		pubkey = message_dict["pubkey"]
		sql = ''' INSERT INTO users(id,password_hash,password_salt,pubkey)
				          VALUES(?,?,?,?) '''
		try:
			cur = db_conn.cursor()
			cur.execute(sql, (name, hash, salt, pubkey))
			db_conn.commit()
		except:
			print("Registration failed")


if __name__ == "__main__":
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-p", "--port", type=int, help="Specifies port")
	parser.add_argument("-k", "--privkeyfile", type=str, help="Specifies private key file")
	args = parser.parse_args()

	server = Server(args.port, args.privkeyfile)
	server.start()