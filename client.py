from Log import *
from threading import Thread
import socket
import os
import json
import hashlib, uuid
import rsa
from Crypto.Cipher import AES
import math
import base64


class Client:

	def __init__(self, server: str, server_port: int, server_pubkeyfile: str, ip: str, port: int):
		Log.add("Client creation: server " + server + "; ip: " + ip + "; port: " + str(port))
		self.__server = server
		self.__server_port = server_port
		self.__ip = ip
		self.__port = port
		self.__messages = []
		self.__contacts = []
		self.__pubkey_server = rsa.PublicKey.load_pkcs1_openssl_pem(open(server_pubkeyfile).read())
		self.__waiting_answer = False
		self.__username = None
		self.__socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	def connect(self):

		self.__socket.bind(('', self.__port))

		login = input("Enter your login: ")

		message = json.dumps({"type": "connect", "login": login, "port": self.__port})

		aes_key = os.urandom(16)
		aes_suite = AES.new(aes_key, AES.MODE_CBC, '0123456789abcdef')

		Log.add("aes key: " + str(aes_key))

		length = math.ceil(len(message) / 16) * 16
		encrypted_message = aes_suite.encrypt(message.ljust(length, '0'))
		encrypted_key = rsa.encrypt(aes_key, self.__pubkey_server)

		self.__socket.sendto(json.dumps({"message": encrypted_message.hex(), "key": encrypted_key.hex()}).encode(),
		            (self.__server, self.__server_port))

		data, addr = self.__socket.recvfrom(65507)

		print("message: " + str(data.decode()))




		self.__thread_receive = Thread(target=self.__thread_receive_fct)
		self.__thread_receive.start()

	def register(self):
		login = input("Enter your login: ")
		password = input("Enter your password: ")
		pubkey_file = input("Enter your public key file: ")
		# sign_file = input("Enter your signature file: ")
		# Take hash of password
		salt = uuid.uuid4().hex
		hashed_password = hashlib.sha512(str.encode(password) + str.encode(salt)).hexdigest()
		hashed_password = hashlib.sha512(str.encode(password) + str.encode(salt)).hexdigest()
		pubkey = open(pubkey_file, 'rb').read().decode()
		# sign = open(sign_file, 'rb').read()

		message = json.dumps(
			{"type": "register", "login": login, "hashed_password": hashed_password, "salt": salt, "pubkey": pubkey})
		print("message: " + str(message))

		aes_key = os.urandom(16)
		aes_suite = AES.new(aes_key, AES.MODE_CBC, '0123456789abcdef')

		Log.add("aes key: " + str(aes_key))

		length = math.ceil(len(message) / 16) * 16
		encrypted_message = aes_suite.encrypt(message.ljust(length, '0'))
		encrypted_key = rsa.encrypt(aes_key, self.__pubkey_server)

		self.__socket.sendto(json.dumps({"message": encrypted_message.hex(), "key": encrypted_key.hex()}).encode(),
		            (self.__server, self.__server_port))



	def interface(self):
		help = "\nquit (q) : disconnect\n" \
		       "help (h) : show this help message\n" \
		       "register : register at server\n" \
		       "connect : connect to server\n" \
		       "add <user> : add contact\n" \
		       "del <user> : delete contact\n" \
		       "read : read messages \n" \
		       "send <user> <text> : send message"
		message = "Enter your cmd: "
		print(help)
		cmd = input(message).lower()

		while cmd != "q":

			if cmd == "h" or cmd == "help":
				print(help)
			elif cmd == "read":
				print(self.__messages)
			elif cmd == "register":
				self.register()
			elif cmd == "connect":
				self.connect()
			elif cmd.startswith("add "):
				pass
				self.send_command(cmd)
				self.__waiting_answer = True
			elif cmd.startswith("del "):
				pass
				self.send_command(cmd)
				self.__waiting_answer = True
			elif cmd.startswith("send "):
				pass
				self.send_command(cmd)
				self.__waiting_answer = True
			# busy loop until self.__waiting_answer is False
			while True:
				if self.__waiting_answer == False:
					break
			cmd = input(message).lower()

	def add_contact(self):
		pass

	def delete_contact(self):
		pass

	def send_message(self):
		pass

	def __thread_receive_fct(self):
		self.__socket.bind(('', self.__port))
		while self.__is_running:
			data, addr = self.__socket.recvfrom(65507)

			print("addr: " + str(addr))

			data_dict = json.loads(data.decode())
			message = bytes.fromhex(data_dict["message"])
			key = bytes.fromhex(data_dict["key"])

			aes_key = rsa.decrypt(key, self.__key)
			Log.add("key decrypted: " + str(aes_key))

			aes_suite = AES.new(aes_key, AES.MODE_CBC, '0123456789abcdef')
			decrypted_message = aes_suite.decrypt(message).decode()
			message_dict = json.loads(decrypted_message.rstrip("0"))
			Log.add("Message: " + str(message_dict))

			type = message_dict["type"]

			if type == "register":
				name = message_dict["login"]
				hash = message_dict["hashed_password"]
				salt = message_dict["salt"]
				pubkey = message_dict["pubkey"]

				sql = ''' INSERT INTO users(id,password_hash,password_salt,pubkey)
				          VALUES(?,?,?,?) '''
				try:
					cur = db_conn.cursor()
					cur.execute(sql, (name, hash, salt, pubkey))
					db_conn.commit()
				except:
					print("Registration failed")

			elif type == "connect":
				print("hello")

if __name__ == "__main__":
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument("-s", "--server", type=str, help="Specifies server IP")
	parser.add_argument("-sp", "--server_port", type=int, help="Specifies server port")
	parser.add_argument("-sk", "--server_pubkey", type=str, help="Specifies server pubkey file")
	parser.add_argument("-i", "--ip", type=str, help="Specifies own IP")
	parser.add_argument("-p", "--port", type=int, help="Specifies port")
	args = parser.parse_args()

	client = Client(args.server, args.server_port, args.server_pubkey, args.ip, args.port)
	client.interface()
